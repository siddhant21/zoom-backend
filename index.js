require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const crypto = require('crypto')
const cors = require('cors')
const KJUR = require('jsrsasign')
const {
  default: axios
} = require('axios')
const app = express()
const port = process.env.PORT || 4000

app.use(bodyParser.json(), cors())
app.options('*', cors())

async function getToken() {
  const auth = Buffer.from('vTa7fdfnRCmne1Z2Q_0Erw:XnxBBiis5o8pNQ25SwrO5nnDS8bi6Ked').toString('base64');
    const atData = await axios.post('https://zoom.us/oauth/token?grant_type=account_credentials&account_id=gEORdq0nQDyEAw5fIrcITw', {}, {
      headers: {
        'Authorization': `Basic ${auth}`
      }
    });
    const accessToken = atData.data.access_token;
    console.log(atData.data)
    return accessToken;
}

app.post('/', (req, res) => {

  const iat = Math.round(new Date().getTime() / 1000) - 30;
  const exp = iat + 60 * 60 * 2

  const oHeader = {
    alg: 'HS256',
    typ: 'JWT'
  }

  const oPayload = {
    sdkKey: '21QeFnp6Pz4vRTHsQuN9h0Z0lY3qORiuVCi9',
    mn: req.body.meetingNumber,
    role: req.body.role,
    iat: iat,
    exp: exp,
    appKey: '21QeFnp6Pz4vRTHsQuN9h0Z0lY3qORiuVCi9',
    tokenExp: iat + 60 * 60 * 2
  }

  const sHeader = JSON.stringify(oHeader)
  const sPayload = JSON.stringify(oPayload)
  const signature = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, 'xmrfb9KiD4HPCcU01ICTFJfqZ9hoZbQyGvYK')

  res.json({
    signature: signature
  })
});
app.post('/webhook', (req, res) => {

  console.log(JSON.stringify(req.body))
  res.json({});
});
app.get('/token', async (req, res) => {
  try {
    // const {
    //   header,
    //   body
    // } = req.body;
    const auth = Buffer.from('vTa7fdfnRCmne1Z2Q_0Erw:XnxBBiis5o8pNQ25SwrO5nnDS8bi6Ked').toString('base64');
    const at = await axios.post('https://zoom.us/oauth/token?grant_type=account_credentials&account_id=gEORdq0nQDyEAw5fIrcITw', {}, {
      headers: {
        'Authorization': `Basic ${auth}`
      }
    });
    console.log(at.data)
    res.json(at.data);
  } catch (error) {
    console.log(error)
  }

});

app.post('/create-meeting', async (req, res) => {
  try {
    const accessToken = await getToken();
    const {meetings} = req.body;
    console.log('meetings', meetings);
    // await deleteMeetings(meetings);


    const meetingData = await axios.post('https://api.zoom.us/v2/users/gaurav.gupta1@pw.live/meetings', {
      // settings: {
      //   meeting_authentication: true
      // }
    }, {
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    });

    // console.log(meetingData);
    res.json(meetingData.data)
  } catch (error) {
    console.log(error)
  }
});
app.get('/list-meetings', async (req, res) => {
  try {
    const accessToken = await getToken();
    const {userId, type} = req.query;
    console.log(userId)
    const meetingData = await axios.get(`https://api.zoom.us/v2/users/${userId}/meetings?type=live`, {
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    })
    console.log(meetingData);
    res.json(meetingData.data);
    
  } catch (error) {
    console.log(error)
  }
});

// async function deleteMeetings(meetingIds) {
//   try {
//     const accessToken = await getToken();
//     console.log('delete meeting', meetingIds)
//     if(meetingIds.length) {
//       await Promise.all(meetingIds.map( async (id) => {
//         await axios.delete(`https://api.zoom.us/v2/meetings/${id}`, {
//           headers: {
//             'Authorization': `Bearer ${accessToken}`
//           }
//         })
//       }))
//     }
//     return;
//   } catch (error) {
//     console.log(error)
//   }

// }
app.listen(port, () => console.log(`Zoom Meeting SDK Sample Signature Node.js on port ${port}!`))